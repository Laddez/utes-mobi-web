$.fn.promoOffers = function(options) {

    options = Object.assign({
		templateSelector: '',
		data: [],
		geoLocation: null,
		showItems: 3
    }, options);

    var $offersList = $(this);
    var $template = $($(options.templateSelector).html());
    initOffers();


    function initOffers() {
    	var data = options.data;
    	if (options.geoLocation) {
    		data = $.grep(data, function(n) {
    			return n.Location.toUpperCase().indexOf(options.geoLocation.toUpperCase()) !== -1;
    		})
    	}

    	if (data.length < options.showItems) {
    		var randomOffers = $.grep(options.data, function(n) {
    			return data.indexOf(n) === -1;
			})

    		data = data.concat(randomOffers.slice(0, options.showItems - data.length));
    	}

        data = data.slice(0, options.showItems);
    	$offersList.empty();
    	$.each(data, renderOffer);
    }

    function renderOffer(i, offer) {
    	var $offer = $template.clone();
    	$offer.find('.js-offer-name').text(offer.Title);
    	$offer.find('.js-offer-image').attr('src', 'http://admin.utes.mobi/' + offer.Picture.path);
    	$offer.find('.js-offer-list').html($.map(offer.Options, function(n){
            return '<li>' + n + '</li>';
           }))
        $offer.find('.js-offer-link').attr('href', offer.Link);

    	$offersList.append($offer);
    }
}








