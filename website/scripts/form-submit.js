$(function() {
    $('#contactForm').submit(function() {
        var $form = $(this);

        if ($form.valid()) {
            var formData = $form.serialize();
            var contactModal = $('#contactModal');
            $.ajax({
                type: 'POST',
                url: $form.attr('action'),
                data: formData,
                success: function(data) {
                    $('#contactForm').trigger('reset');
                    setTimeout(function() { contactModal.modal(); }, 500);
                    setTimeout(function() { contactModal.modal('hide'); }, 5000);
                },
                error: function(xhr, str) {
                    console.log('Error: ' + xhr.responseCode);
                }
            });
        }

        return false;
    })
})