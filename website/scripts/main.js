$(function() {
    // var dataSet = [];
    // getData();

    if (document.referrer.indexOf(location.hostname) === -1) {
        var currentLanguage = $('html').attr('lang');
    
        if (navigator.languages.indexOf('pl') >= 0 && currentLanguage != 'pl') {
            window.location.href = '/utes/pl';
        } 
    }

    var scrollToTarget = function() {
        var $target = $(location.hash);
        var navbarHeight = $('.navbar-header').height();
        if ($target.length) {
            var targetPos = $target.offset().top;
            $(window).scrollTop(targetPos - navbarHeight);
        }
    }

    scrollToTarget();

    
            $('[data-page-scroll]').bind('click', function(event) {
                var target = $(this).attr('data-page-scroll') || $(this).attr('href');
                var $target = $(target);
                var $navbar = $('.navbar-fixed-top');
                var $navbarHeader = $('.navbar-header');
        
                if (!$target.length) {
                    window.location.href = '/' + target;
                }
        
                var scrollTo = $target.offset().top - ($navbar.length ? $navbarHeader.height() : 0);
        
                if ($target.length) {
                    $(".navbar-nav li a").click(function(event) {
                        $(".navbar-collapse").collapse('hide');
                    });
        
                    $('html, body').stop().animate({
                        scrollTop: (scrollTo)
                    }, 1000, 'easeInOutExpo');
                }
                event.preventDefault();
                return false;
            });
        

    $('#main-navbar li a').bind('click', function() {
        $('#main-navbar').collapse('hide');
    });

    window.sr = ScrollReveal();
    sr.reveal(".js-title-left", {
        duration: 600,
        origin: "left",
        distance: "400px",
        mobile: false,
        viewFactor: .7
    }, 400);

    sr.reveal(".js-title-right", {
        duration: 800,
        origin: "right",
        distance: "400px",
        mobile: false,
        delay:1500,
        viewFactor: .7
    }, 400);

    sr.reveal(".about-text", {
        delay: 400,
        duration: 800,
        scale: 0.7,
        mobile: false,
        viewFactor: .7
    });


    sr.reveal(".js-title-bottom", {
        delay: 200,
        duration: 600,
        original: 'bottom',
        distance: '100px',
        mobile: false,
        viewFactor: 1
    });


    var token = "5854c8421361dd2704dde9102affc5";

    $.get("https://ipinfo.io", function(response) {
        // console.log(response.country)
        var currentCountry = response.country;
        var options = {token: token, filter: {Active: true}};
        $.get('http://admin.utes.mobi/api/collections/get/Offers', options)
            .then(function(result) {
                $('#offersContainer').promoOffers({
                    templateSelector: "#offer-template",
                    data: result.entries,
                    geoLocation: currentCountry
                });
                
                $('.transition-fade').addClass('in');
            });
    }, "jsonp");


});