var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');
var svgo = require('gulp-svgo');
var rigger = require('gulp-rigger');
var htmlmin = require('gulp-htmlmin');
var flatten = require('gulp-flatten');

var browserSync = require('browser-sync').create();

var sourceDir = 'website';
var buildDir = 'build';

gulp.task('html', function() {
    gulp.src([sourceDir + '/**/*.html', '!' + sourceDir + '/parts/*'])
        .pipe(rigger())
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest(buildDir + '/'))
        .on('end', function() {
            browserSync.reload();
        })
})

gulp.task('css', function() {
    gulp.src(sourceDir + '/styles/main.scss')
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: [sourceDir + '/styles']
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(buildDir + '/styles/'))
        .pipe(browserSync.stream());
});



gulp.task('js', function() {
    gulp.src(sourceDir + '/scripts/*.js')
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(buildDir + '/scripts/'))
        .pipe(browserSync.stream());
});


gulp.task('img', function() {
    gulp.src(sourceDir + '/images/**')
        .pipe(gulp.dest(buildDir + '/images'));
});

gulp.task('fonts', function() {
    gulp.src(sourceDir + '/fonts/**')
        .pipe(gulp.dest(buildDir + '/fonts'));
});

gulp.task('video', function() {
    gulp.src(sourceDir + '/video/**')
        .pipe(gulp.dest(buildDir + '/video'));
});


gulp.task('staff', function() {
    gulp.src([
            sourceDir + '/**/*.config',
            sourceDir + '/**/.htaccess',
            sourceDir + '/**/*.php',            
            sourceDir + '/**/*.xml'
        ])
        .pipe(gulp.dest(buildDir));
});


gulp.task('vendors:js', function() {
    gulp.src([
            sourceDir + '/vendors/jquery/jquery-3.1.0.min.js',
            sourceDir + '/vendors/**/*.js'
        ])
        .pipe(concat('vendors.min.js'))
         .pipe(uglify())
        .pipe(gulp.dest(buildDir + '/scripts/'));
});

gulp.task('vendors:css', function() {
    gulp.src([sourceDir + '/vendors/**/*.css'])
        .pipe(concat('vendors.min.css'))
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(buildDir + '/styles/'));
});

gulp.task('vendors:fonts', function() {
    gulp.src([
            sourceDir + '/vendors/**/*.eot',
            sourceDir + '/vendors/**/*.svg',
            sourceDir + '/vendors/**/*.ttf',
            sourceDir + '/vendors/**/*.woff',
            sourceDir + '/vendors/**/*.woff2'
        ])
        .pipe(flatten())
        .pipe(gulp.dest(buildDir + '/fonts/'));
});

gulp.task('watch', ['default'], function() {
    browserSync.init({
        server: {
            baseDir: buildDir,
            serveStaticOptions: {
                extensions: ['html']
            }
        },
        port: 3012
    });

    gulp.watch(sourceDir + '/styles/*.scss', ['css']);
    gulp.watch(sourceDir + '/**/*.html', ['html']);
    gulp.watch(sourceDir + '/scripts/*.js', ['js']);
    gulp.watch(sourceDir + '/images/**', ['img']);
    gulp.watch(sourceDir + '/video/**', ['video']);
    gulp.watch(sourceDir + '/**/*', ['staff']);
});

gulp.task('default', ['css', 'js', 'html', 'img', 'fonts', 'video', 'staff', 'vendors:js', 'vendors:css', 'vendors:fonts']);